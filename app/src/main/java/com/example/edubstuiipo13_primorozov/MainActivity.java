package com.example.edubstuiipo13_primorozov;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.IntegerRes;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.StringTokenizer;

public class MainActivity extends Activity {
    private TextView emailTv;
    private TextView mobileTv;
    private Button callStudent;
    private String TAG="LifeCycle";
    Intent intent , choose;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        emailTv=(TextView)findViewById(R.id.email);
        mobileTv=(TextView)findViewById(R.id.mobile);
        callStudent=(Button)findViewById(R.id.callStudent);
        callStudent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_DIAL);
                String p = "tel:" +mobileTv.getText().toString();
                i.setData(Uri.parse(p));
                startActivity(i);
            }
        });
        emailTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent=new Intent(Intent.ACTION_SEND);
                intent.setData(Uri.parse("mailto:"));
                String to=emailTv.getText().toString();
                intent.putExtra(Intent.EXTRA_EMAIL,to);
                intent.putExtra(Intent.EXTRA_SUBJECT,"Lab1Test");
                intent.putExtra(Intent.EXTRA_TEXT,"Hello , this is a test message!");
                intent.setType("message/rfc822");
                choose=Intent.createChooser(intent,"Send Email!");
                startActivity(choose);
            }
        });
        Log.d(TAG,"Активити onCreate()");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG,"Активити onStart()");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG,"Активити onStop()");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG,"Активити onDestroy()");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG,"Активити onResume()");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(TAG,"Активити onRestart()");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG,"Активити onPause()");
    }
}
